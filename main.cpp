#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    // On définit le nom de l'app proprement
    QApplication::setApplicationName("Editeur de fiches de lecture");

    MainWindow w;
    // On définit le nom de la fenêtre principale comme le nom de l'app
    w.setWindowTitle(QCoreApplication::applicationName());
    w.show();

    return a.exec();
}
