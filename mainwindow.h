#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    // Définition des slots privés (boutons pressés, lâchés...).
    // Autodéfinis quand on clique sur "Aller au slot" depuis l'éditeur d'interface.
    // https://doc.qt.io/qt-5/signalsandslots.html

    // Boutons
    void on_boutonNouveau_released();
    void on_boutonEnregistrer_released();
    void on_boutonSupprimer_released();
    void on_boutonQuitter_released();
    void on_boutonAfficher_released();
    void on_boutonAide_released();

    // Fonctions compagnons
    void nouvelle_fiche();                  // On déclare ici la fonction custom parce qu'elle est fait partie de MainWindow
    void msg_confirmation(QString custom);  // Confirmer la fermeture

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
