﻿/* Projet récemment ajouté sur Git : https://framagit.org/gui/cartesmem-qt */

#include <QFileDialog>  // Boite de dialogue pour choisir un fichier
#include <QDir>         // Pour trouver facilement le homepath quel que soit l'OS
#include <QTextStream>  // Flux d'un fichier ouvert
#include <QtXml>        // Gestion XML, il faut aussi l'ajouter dans le .h
#include <QMessageBox>  // Pour prévenir si ça rate
#include <QDebug>

#include "mainwindow.h"
#include "ui_mainwindow.h"

QDir ourPath(){
    // Fonction qui rérifie que notre path home/fiches existe,
    //  le créé sinon, et le retourne.
    QDir home = QDir::homePath();           // Récupérer le home path
    QString dirstr = home.absolutePath();   // En faire un string
    dirstr += "/fiches";                    // Ajouter /fiches
    QDir dir(dirstr);                       // En refaire un QDir

    if (!dir.exists()) {                    // Si ce path /fiches n'existe pas,
        QDir().mkdir(dirstr);               // le créer
    }

    return dir;
}

int firstAvailableNumber(){
    int x = 1;
    QFileInfoList list = ourPath().entryInfoList();     // La liste des fichiers dans le réertoire
    for (int i = 0; i < list.size(); ++i) {             // Pour chaque fichier
        QFileInfo fileInfo = list.at(i);
        QString fileName = fileInfo.fileName();
        if (fileName.length() == 10){                   // Vérifier que c'est bien un de nos fichiers;
            QStringList split = fileName.split("");     // Spliter en un tableau
            if (split[6].toInt() != x) {    // Si on a trouvé un nombre libre
                return x;                               // On le retourne
            }
            else x++;                                   // Sinon, on essaye le prochain
        }
    }
    return x;                                           // S'il n'y a aucun fichier dans le dossier
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {

    ui->setupUi(this);

    QFileInfoList list = ourPath().entryInfoList();     // La liste des fichiers dans le réertoire
    for (int i = 0; i < list.size(); ++i) {             // Pour chaque fichier
        QFileInfo fileInfo = list.at(i);
        if (fileInfo.fileName() != "." && fileInfo.fileName() != ".."){ // Eviter les liens symbolics
            ui->listeFiches->addItem(fileInfo.fileName());  // On ajoute à la liste de l'ui
        }
    }

}

MainWindow::~MainWindow() {
    // Destructeur de MainWindow
    delete ui;
}

void sauvegarder(QString string, QString filename) {
    // Sauvegarde dans le repertoire home/fiches/filename un QString en argument.

    QDir dir = ourPath();                   // Récupérer notre path

    QFile fichier(dir.filePath(filename));  //  Path du fichier
        // Si l'on peut bien écrire dans ce fichier
    if (fichier.open(QIODevice::WriteOnly | QIODevice::Text)){
        QTextStream flux(&fichier);         // On créé le flux
        flux << string;                     // On y met notre texte
        fichier.close();                    // On ferme
    }
    else {
        QMessageBox msgBox;
        msgBox.setText("Il y a eu une erreur, veuillez réessayer.");
        msgBox.exec();
    }
}

void MainWindow::nouvelle_fiche(){
    // Créer un fichier vide. Comme elle fait partie de MainWindow, cette fonction a aussi accès à ui et this.
    QDomDocument doc;
    QDomElement root = doc.createElement("Fiche");
    doc.appendChild(root);
    QDomElement node = doc.createElement("Fiche");
    node.setAttribute("Titre", "");
    node.setAttribute("Auteur", "");
    node.setAttribute("Resume", "");
    root.appendChild(node);

    QString fileName = "fiche" + QString::number(firstAvailableNumber()) + ".xml";  // Nom de fichier
    sauvegarder(doc.toString(), fileName);              // Enregistrer le fichier vide
    ui->listeFiches->addItem(fileName);                 // On ajoute à la liste de l'ui
    int index = ui->listeFiches->findText(fileName);    // Trouver l'index de l'élément fraîchement ajouté
    if (index != -1) {                                  // -1 si non trouvé (erreur)
        ui->listeFiches->setCurrentIndex(index);        // Définir l'index courant
    }
}

void MainWindow::on_boutonNouveau_released() {
    // Effacer les champs lorsqu'on veut une nouvelle fiche
    //! Il faudrait demander à l'utilisateur s'il souhaite effacer les champs sans enregistrer.
    ui->ligneTitre->clear();
    ui->ligneAuteur->clear();
    ui->texteResume->clear();

    // Séparer la nouvelle fiche permet de ne nettoyer les champs que pour le bouton Nouveau
    nouvelle_fiche();
}

void MainWindow::on_boutonEnregistrer_released() {
    // Enregistrer le texte dans un fichier

    // Demander à l'utilisateur dans quel fichier enregistrer :
    /*QString source = QFileDialog::getSaveFileName(this,
                                                  tr("Enregistrer dans un fichier"),
                                                  QDir::homePath());*/

    // Préparer le XML
    QDomDocument doc;
    QDomElement root = doc.createElement("Fiche");
    doc.appendChild(root);
    QDomElement node = doc.createElement("Fiche");
    node.setAttribute("Titre", ui->ligneTitre->text());
    node.setAttribute("Auteur", ui->ligneAuteur->text());
    node.setAttribute("Resume", ui->texteResume->toPlainText());
    root.appendChild(node);

    // Créer une fiche s'il n'y en a aucune
    QVariant currentval = ui->listeFiches->currentText();   // Récupérer le texte de l'item courrant
    QString currentstr = currentval.toString();
    if (currentstr.length() == 0) {                         // Si le texte de la QComboBox est vide
        nouvelle_fiche();                                   // On créé automatiquement une nouvelle fiche (sans vider les champs)

        // On récupère le nouveau texte
        currentval = ui->listeFiches->currentText();
        currentstr = currentval.toString();
    }

    sauvegarder(doc.toString(), currentstr);
}

void MainWindow::on_boutonSupprimer_released() {
    // Supprimer une fiche

    // Effacer les champs
    ui->ligneAuteur->clear();
    ui->ligneTitre->clear();
    ui->texteResume->clear();

    // Supprimer le fichier
    QVariant currentval = ui->listeFiches->currentText();   // Récupérer le texte de l'item courrant
    QDir currentdir = ourPath();                            // Récupérer le path du projet
    QString fileName = currentdir.absoluteFilePath(currentval.toString());  // Concaténer avec le path
    QFile fichier(fileName);
    fichier.remove();

    // Effacer la fiche de la QComboBox
    ui->listeFiches->removeItem(ui->listeFiches->currentIndex());

    // Informer de la suppression
    QMessageBox msgBox;
    msgBox.setText("Fiche supprimée.");
    msgBox.exec();
}

void MainWindow::msg_confirmation(QString custom) {
    QMessageBox::StandardButton rep;
    rep = QMessageBox::question(this, "N'oubliez par de sauvegarder !", custom+"Voulez-vous vraiment quitter ?",
                                  QMessageBox::Yes|QMessageBox::No);
    // Pour afficher les boutons en français : https://stackoverflow.com/questions/35887523/qmessagebox-change-text-of-standard-button
    // Mais le code est long pour pas grand chose.
    if (rep == QMessageBox::Yes) {
      this->close();
      QApplication::quit();
    }
}

void MainWindow::on_boutonQuitter_released() {
    // Fermer lorsqu'on appuie sur le bouton homonyme
    QVariant currentval = ui->listeFiches->currentText();   // Récupérer le texte de l'item courrant
    QString currentstr = currentval.toString();

    if (currentstr.length() != 0) {                         // Si la combobox n'est pas vide
        QDir currentdir = ourPath();                        // Récupérer le path du projet
        QString fileName = currentdir.absoluteFilePath(currentval.toString());  // Concaténer avec le path

        QFile fichier(fileName);
        QDomDocument doc; // XML

        // Si l'ouverture s'est bien passée
        if (fichier.open(QIODevice::ReadOnly | QIODevice::Text)){

            // Récupérer le XML
            doc.setContent(&fichier);
            QDomElement root = doc.firstChildElement();

            QDomNodeList items = root.elementsByTagName("Fiche");
            QDomNode item = items.at(0); // Ici, on pourra facilement mettre plusieurs fiches dans le même fichier
            QDomElement element = item.toElement();

            // Vérifier si le texte dans l'éditeur est le même que dans le fichier.
            if(ui->ligneTitre->text() == element.attribute("Titre")
                && ui->ligneAuteur->text() == element.attribute("Auteur")
                && ui->texteResume->toPlainText() == element.attribute("Resume")) {
                fichier.close();
                this->close();
            }
            else {                                          // Si ce n'est pas le cas, on demande confirmation
                fichier.close();
                msg_confirmation("Vous n'avez pas sauvegardé vos modifications. ");
            }

        }
        else {                                              // L'ouverture s'est mal passée, on demande confirmation
            msg_confirmation("Le fichier de sauvegarde a peut-être été supprimé. ");
        }
    }
    else {                                                  // La combobox est vide
        if(ui->ligneTitre->text() == "" && ui->ligneAuteur->text() == "" && ui->texteResume->toPlainText() == ""){
            this->close();                                  // Si les champs sont vides, on ferme la fenêtre
        }
        else {                                              // Sinon, on demande.
            msg_confirmation("Vous n'avez pas sauvegardé vos modifications. ");
        }

    }
}



void MainWindow::on_boutonAfficher_released() {
    // Afficher le texte d'un élément choisit

    /*// Boite de dialogue :
    QString source = QFileDialog::getOpenFileName(this,
                                                  "Ouvrir un fichier",
                                                  QDir::homePath());*/

    QVariant currentval = ui->listeFiches->currentText();   // Récupérer le texte de l'item courrant
    QString currentstr = currentval.toString();
    if (currentstr.length() == 0) {                         // S'il n'y a rien dans la QComboBox
        QMessageBox msgBox;
        msgBox.setText("Il n'y a aucune fiche à afficher.");
        msgBox.exec();
    }
    else {
        QDir currentdir = ourPath();                        // Récupérer le path du projet
        QString fileName = currentdir.absoluteFilePath(currentval.toString());  // Concaténer avec le path

        QFile fichier(fileName);
        QDomDocument doc; // XML

        // Si l'ouverture s'est bien passée
        if (fichier.open(QIODevice::ReadOnly | QIODevice::Text)){

            // Récupérer le XML
            doc.setContent(&fichier);
            QDomElement root = doc.firstChildElement();

            QDomNodeList items = root.elementsByTagName("Fiche");
            /* Ici, on pourrait facilement mettre plusieurs fiches dans le même fichier.
             * J'ai choisi d'utiliser des fichiers différents pour respecter la consigne. */
            QDomNode item = items.at(0);
            QDomElement element = item.toElement();

            ui->ligneTitre->setText(element.attribute("Titre"));
            ui->ligneAuteur->setText(element.attribute("Auteur"));
            ui->texteResume->setPlainText(element.attribute("Resume"));

            fichier.close();
        }
        else {
            QMessageBox msgBox;
            msgBox.setText("Il y a eu une erreur, veuillez réessayer.");
            msgBox.exec();
        }
    }
}

void MainWindow::on_boutonAide_released() {
    // Bouton qui propose une explication du fonctionnement
    QMessageBox msgBox;
    msgBox.setText("Bienvenue dans votre gestionnaire de fiches de lectures.\n"
                   "Commencez par créer une nouvelle fiche avec 'Nouveau', celle-ci sera stockée dans votre dossier personnel, sous-dossier 'fiches'.\n"
                   "Après avoir écrit dans votre fiche, n'oubliez pas d'enregistrer les modifications avec le bouton homonyme.\n"
                   "Vous pouvez sélectionner une de vos fiches dans le menu déroulant, puis cliquer sur 'Afficher' pour la voir.");
    msgBox.exec();
}
